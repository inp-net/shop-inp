import { config } from 'dotenv';
import { resolve } from 'path';

// Specify the root .env location
config({ path: resolve(process.cwd(), '../.env') });