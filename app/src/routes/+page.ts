import type { PageLoad } from './$types';
import { PUBLIC_API_URL } from '$env/static/public';

export const load: PageLoad = async ({ fetch }) => {
    const response = await fetch(PUBLIC_API_URL);
    const data = await response.json();
    return {
        props: {
            data
        }
    };
};