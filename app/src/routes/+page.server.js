import { products } from '$lib/data/data.js';

export function load() {
	return {
		summaries: products.map((prod) => ({
			slug: prod.slug,
			title: prod.title,
            images: prod.images,
            price: prod.price
		}))
	};
}