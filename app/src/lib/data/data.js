import placeholder1 from '$lib/assets/sweat-placeholder-1.jpeg';
import placeholder2 from '$lib/assets/sweat-placeholder-2.jpeg'

export const products = [
	{
		slug: 'welcome',
		title: 'Sweat à Capuche',
        price: 35,
        images: [placeholder1, placeholder2],
		content:
			'<p>We hope your brief detention in the relaxation vault has been a pleasant one.</p><p>Your specimen has been processed and we are now ready to begin the test proper.</p>'
	},

	{
		slug: 'safety',
		title: 'Sweat à Capuche',
        price: 30,
        images: [placeholder1, placeholder2],
		content:
			'<p>While safety is one of many Enrichment Center Goals, the Aperture Science High Energy Pellet, seen to the left of the chamber, can and has caused permanent disabilities, such as vaporization. Please be careful.</p>'
	},

	{
		slug: 'cake',
		title: 'Sweat à Capuche',
        price: 30, 
        images: [placeholder1, placeholder2],
		content: "<p>I'm making a note here: HUGE SUCCESS.</p>"
	}
];