import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

import './src/scripts/dotenv.ts';

export default defineConfig({
	plugins: [sveltekit()]
});
