import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'shop.inp',
  appName: 'shop-inp',
  webDir: 'www'
};

export default config;
