# Shop-INP

Répo principal du projet Shop-INP.

La maquette peut être consultée [ici](https://www.figma.com/design/zoTw0UHCPi1VSbxkZjowsN/Shop-Inp?node-id=0-1&t=9ZTjpZtkZHAO6BS3-1)

## Installation

### Prérequis

- volta (https://volta.sh/)
- Docker Compose (https://docs.docker.com/compose/install/)

### Installation

Commencer par cloner le répo :
```bash
git clone https://git.inpt.fr/shop-inp/shop-app.git
```

Une fois dans le repo, installer les dépendances :
```bash
yarn install
```

Copier le fichier `.env.example` en `.env` :
```bash
cp .env.example .env
```

Build le projet :
```bash
yarn build
```

Lancer le projet :
```bash
yarn dev
```

## Contribuer

Pour contribuer, vous pouvez fork le projet et créer une pull request.

### Note pour l'API
Le répo se sépare en deux parties : le frontend sous le dossier `app` et le backend dans le sous-module `api`.
Pour modifier l'API il faut d'abord commit dans le sous module `api` (voir la documentation git pour les sous modules)
Puis il faut commit dans le répo principal pour que les changements soient pris en compte.

